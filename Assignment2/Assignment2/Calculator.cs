﻿using System;
namespace Assignment2
{
    public class Calculator
    {
        int[] numbers; 
        public Calculator(int[]numbers)
        {
            this.numbers = numbers;
        }
        public int Sum() {
            int result = 0;
            foreach (int num in numbers) { result += num; }
            return result;
        }
        public int Subtraction() {

            /* make another solution for this and if the number
             * becomes negative break the for loop and return 0 
            */
            int result = numbers[0];
            foreach (int num in numbers) { result -= num; }
            return result+numbers[0];
        }
        public int Division(int divideby) {
            /* handle the following scenarios
             * if the numbers array was empty
             * the divideby was zero
            */
            return Sum() / divideby;
        }
        public int Multiplication(int multiplyby) {
            // instead of division make if(multiplication)
            return Sum() / multiplyby;
        }
    }
}
